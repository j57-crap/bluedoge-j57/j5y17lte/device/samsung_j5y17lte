```
Copyright (C) 2017-2019 The LineageOS Project
```


Device configuration for the Samsung Galaxy J5 2017 (J530F/Y/FM/xx)
=================================================================

## Device specifications ## 

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | 1,6 GHz octa-core
Chipset | Samsung Exynos 7870
GPU     | Mali-T830 MP1
Memory  | 2 GB RAM
Shipped Android Version | 7.0 upgradable to 8.1
Storage | 16 GB
Battery | Li-Ion 3000mAh battery
Display | 720 x 1080 pixels, 5.2", SUPER AMOLED 
Camera  | 13 MP rear cam and 13 MP front cam
Other capabilities | NFC, Audio stream
Sensors | Accelerometer, Proximity, FingerPrint, Gyroscope, compass, barometer

## Device Preview

![Samsung Galaxy J5 2017](https://cdn2.gsmarena.com/vv/pics/samsung/samsung-galaxy-j5-2017-sm-j530-2.jpg "J5 2017 in black")
